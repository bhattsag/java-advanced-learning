package collection.set;

import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;

public class SetExample1 {

    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("hello");
        set.add("world");
        set.add("world");
        set.add(null);
        System.out.println(set);

        Set<String> set1 = new HashSet<>(Arrays.asList(new String[] {"abc", "def", "hello"}));

        Set<String> set2 = new HashSet<>(set);
        set2.addAll(set1);
        System.out.println(set2);

        //Integer
        Set<Integer> set3 = new HashSet<>(Arrays.asList(1,5,7,9,22,25,1,3,5,11,22,33));
        Set<Integer> set4 = new HashSet<>(Arrays.asList(11,15,17,19,122,125,1,3,5,11,22,33));
        Set<Integer> set5 = new HashSet<>(set3);
        set5.addAll(set4);
        System.out.println(set5);

    }
}
