package collection.linklist;

import java.util.Iterator;
import java.util.LinkedList;

public class LinkListExample {

    public static void main(String[] args) {
        LinkedList<String> ll = new LinkedList<>();
        ll.add("test1");
        ll.add("test2");
        ll.add("test3");
        ll.addFirst("Added at first");
        ll.addLast("Added at last");
        System.out.println(ll);
        System.out.println(ll.get(1));
        //Iterating in forward direction
        Iterator<String> it = ll.iterator();
        while (it.hasNext()){
            System.out.println(it.next());
        }
        //Iterating in backward direction
        Iterator<String> itr = ll.descendingIterator();
        while (itr.hasNext()){
            System.out.println(itr.next());
        }

        //Iterating using for loop
        for(String s: ll){
            System.out.println(s);
        }
        ll.removeFirst();
        ll.removeLast();
        System.out.println(ll);
    }
}
