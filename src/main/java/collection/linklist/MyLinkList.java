package collection.linklist;

public class MyLinkList {
    Node head;

    class Node{
        int data;
        Node next;
        Node(int data){
            this.data = data;
            next = null;
        }
    }

    public void printNodeList(){
        Node n = head;
        while (n!=null){
            System.out.println(n.data);
            n = n.next;
        }
    }

    public static void main(String[] args) {
        MyLinkList myLinkList = new MyLinkList();
        Node first = myLinkList.new Node(10);
        myLinkList.head = first;
        Node second = myLinkList.new Node(20);
        first.next = second;
        Node third = myLinkList.new Node(30);
        second.next = third;
        myLinkList.printNodeList();

    }
}
