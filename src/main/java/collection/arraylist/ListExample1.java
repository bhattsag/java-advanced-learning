package collection.arraylist;

import java.lang.reflect.Array;
import java.util.*;

public class ListExample1 {

    public static void main(String[] args) {
        ArrayList<String> crickterNames = new ArrayList<String>();
        //Add values to arraylist
        crickterNames.add("Sachin");
        crickterNames.add("Sehwag");
        crickterNames.add("Rohit");
        crickterNames.add("Harbhajan");
        crickterNames.add(3, "Hardik");

        //Iterate ArrayList using for loop
        for(int i =0;i<crickterNames.size();i++){
            System.out.print(crickterNames.get(i)+",");
        }
        System.out.println();

        //Iterating ArrayList using for each
        for(String s : crickterNames){
            System.out.print(s+",");
        }
        System.out.println();

        //Iterating ArrayList using lamda function
        crickterNames.stream().forEach(player -> System.out.print(player+","));
        System.out.println();

        //Iterating ArrayList using iterator

        Iterator<String> it = crickterNames.iterator();
        while (it.hasNext()){
            System.out.print(it.next()+",");
        }

        System.out.println("\nItearting in backward");
        ListIterator<String> lit = crickterNames.listIterator();
        lit.next();
        while (lit.hasPrevious()){
            System.out.println(lit.previous());
        }

        //Add all method
        ArrayList<String> mumbaiIndian = new ArrayList<>();
        mumbaiIndian.addAll(crickterNames);
        System.out.println(mumbaiIndian);

        //Clear method
        mumbaiIndian.clear();
        System.out.println(mumbaiIndian);

        //Clone arraylist
        ArrayList<String> newCricketer = (ArrayList<String>) crickterNames.clone();
        System.out.println(newCricketer);

        //Contains
        System.out.println(newCricketer.contains("Sachin"));
        System.out.println(newCricketer.contains("Kohli"));

        //Index of
        System.out.println(newCricketer.indexOf("Sachin")); //0
        System.out.println(newCricketer.indexOf("Kohli")); //-1

        //Last Index of
        newCricketer.add("Sachin");
        System.out.println(newCricketer.lastIndexOf("Sachin"));//5

        //Remove
        newCricketer.remove(newCricketer.lastIndexOf("Sachin"));
        System.out.println(newCricketer.lastIndexOf("Sachin"));//0

        //Remove based on condition
        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9,10));
        numbers.removeIf(num -> num % 2 !=0);
        System.out.println(numbers);

        //Sub list
        ArrayList<Integer> subList = new ArrayList<>(numbers.subList(1,5));
        System.out.println(subList);

        //Convert ArrayList to Array
        Object obj [] = newCricketer.toArray();
        System.out.println(Arrays.toString(obj));

        //Retain All
        newCricketer.add("Sachin");
        newCricketer.retainAll(Collections.singleton("Sachin")); //Sachin, Sachin
        System.out.println(newCricketer);


    }
}
