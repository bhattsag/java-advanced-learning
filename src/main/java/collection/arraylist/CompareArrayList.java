package collection.arraylist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class CompareArrayList {

    public static void main(String[] args) {
        //Compare two arraylist
        //1. sort then compare
        ArrayList<String> alphabet1 = new ArrayList<>(Arrays.asList("Z", "A", "C", "F", "G", "R"));
        ArrayList<String> alphabet2 = new ArrayList<>(Arrays.asList("A", "Z", "C", "F", "G", "R"));
        ArrayList<String> alphabet3 = new ArrayList<>(Arrays.asList("A", "Z", "C", "F", "G"));
        Collections.sort(alphabet1);
        Collections.sort(alphabet2);
        Collections.sort(alphabet3);
        System.out.println(alphabet1.equals(alphabet2));//true
        System.out.println(alphabet1.equals(alphabet3));//false

        //2. Compare two list - find out additional elements
        ArrayList<String> alphabet4 = new ArrayList<>(Arrays.asList("Z", "A", "C", "F", "H"));
        ArrayList<String> alphabet5 = new ArrayList<>(Arrays.asList("A", "Z", "C", "F", "G"));
//        alphabet4.removeAll(alphabet5);
//        System.out.println(alphabet4);

        //3. Findout - missing element
        alphabet5.removeAll(alphabet4);
        System.out.println(alphabet5); //G

        //4. Findout - common element
        ArrayList<String> lan1 = new ArrayList<>(Arrays.asList("Java", "Python", "C", "C#", "Javascript"));
        ArrayList<String> lan2 = new ArrayList<>(Arrays.asList("Java", "Python", "C", "C#", "Typescript"));
        lan1.retainAll(lan2);
        System.out.println(lan1);


    }
}
