package collection.arraylist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class SynchronizedList {

    public static void main(String[] args) {

        //Synchronize using sychronizedList method of collections class
        List<String> nameList = Collections.synchronizedList(new ArrayList<String>());
        nameList.add("Java");
        nameList.add("Python");
        nameList.add("C");
        nameList.add("C#");

        // to add, remove - we don't need expliclit synchronization
        //to fetch the value from this list we need to use expliclit synchronization

        synchronized (nameList){
            Iterator<String> it = nameList.iterator();
            while (it.hasNext()){
                System.out.println(it.next());
            }
        }

        //CopyOnWriteArrayList
        CopyOnWriteArrayList<String> employeeName = new CopyOnWriteArrayList<>();
        employeeName.add("Steve");
        employeeName.add("Sagar");
        employeeName.add("Mark");

        //We don't need any expliclit synchronization

        Iterator<String> it = employeeName.iterator();
        while (it.hasNext()){
            System.out.println(it.next());
        }


    }



}
