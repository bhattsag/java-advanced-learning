package collection.arraylist;

import java.util.*;
import java.util.stream.Collectors;

public class RemoveDuplicateElement {

    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<>(Arrays.asList("Hi", "hello", "hey", "Hi", "hey"));
        // Remove duplicate values using linked hash set or hash set
        HashSet<String> hashSet = new HashSet<>(words);
//        LinkedHashSet<String> linkedHashSet = new LinkedHashSet<>(words);
        ArrayList<String> newWords = new ArrayList<>(hashSet);
        System.out.println(newWords);

        //Jdk 8 stream
        ArrayList<Integer> marks = new ArrayList<>(Arrays.asList(50,51,25,25,88,88,90,91,91));
        List<Integer> newMarks = marks.stream().distinct().collect(Collectors.toList());
        System.out.println(newMarks);
    }
}
