package collection.map;

import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MapFlavors {

    public static void main(String[] args) {
        Map<String, String> map1 = Stream.of(new String[][]{
                {"Tom", "A-Grade"},
                {"Lisa", "B-Grade"}
        }).collect(Collectors.toMap(data->data[0], data->data[1]));
        System.out.println(map1.get("Lisa"));

        Map<String, String> map2 = Stream.of(
                new AbstractMap.SimpleEntry<>("Sagar", "QA"),
                new AbstractMap.SimpleEntry<>("Ketan", "Dev")
        ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        System.out.println(map2.get("Sagar"));

        //Empty map
        Map<String, String> emptyMap = Map.of();

        //Single value map
        Map<String, String> singletonMap = Map.of("k1", "v1");
        System.out.println(singletonMap.get("k1"));

        //Multi value map
        Map<String, String> multiMap = Map.of("k1", "v1", "k2", "v2"); //Maximum 10 key values are supported
        //Immutable map
        Map<String, String> map3 = Map.ofEntries(
                new AbstractMap.SimpleEntry<>("A", "1000"),
                new AbstractMap.SimpleEntry<>("B", "2000")
        );



    }
}
