package collection.map;

import java.util.Collections;
import java.util.Map;

public class ImmutableMap {

    public static void main(String[] args) {
        Map<String, String> immutableMap = Collections.singletonMap("Test", "Test-A");
        immutableMap.put("test", "test-b");
    }
}
