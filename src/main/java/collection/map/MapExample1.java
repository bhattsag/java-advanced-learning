package collection.map;

import java.util.*;

public class MapExample1 {

    public static void main(String[] args) {
        Map<String, String> map1 = new HashMap<>();

        map1.put("Gujarat", "Gandhinagar");
        map1.put("Maharashtra", "Mumbai");
        map1.put("Karnataka", "Bangalore");
        map1.put("Rajasthan", "Jaipur");

        //Iterate key set
        System.out.println("Print all keys from Map");
        Iterator<String> key = map1.keySet().iterator();
        while (key.hasNext()){
            System.out.println(key.next());
        }
        //Iterate key set
        System.out.println("Print all values from Map");
        Iterator<String> value = map1.values().iterator();
        while (value.hasNext()){
            System.out.println(value.next());
        }

        //Iterate Key & Value together
        System.out.println("For loop to iterate key-value pair");
        for(Map.Entry e: map1.entrySet()){
            System.out.println(e.getKey()+ "--"+ e.getValue());
        }

        //Iterator method
        Iterator<Map.Entry<String, String>> it = map1.entrySet().iterator();
        System.out.println("Iterator method to iterate key-value pair");
        while(it.hasNext()){
            Map.Entry<String, String> entry = it.next();
            System.out.println(entry.getKey()+"--"+ entry.getValue());
        }

        //Remove elements
        map1.remove("Maharashtra");

        //Iterate using forEach
        map1.forEach((k,v)-> System.out.println("Key is: "+ k + " and value is: "+ v));
    }
}
