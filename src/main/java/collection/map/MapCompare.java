package collection.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class MapCompare {

    public static void main(String[] args) {
        Map<String, String> map1 = new HashMap<>();
        map1.put("A", "Sagar");
        map1.put("B", "Bhatt");
        map1.put("C", "Robert");
        map1.put("D", "Lisa");

        Map<String, String> map2 = new HashMap<>();
        map2.put("A", "Sagar");
        map2.put("B", "Bhatt");
        map2.put("C", "Robert");
        map2.put("D", "Lisa");
        map2.put("E", "Lisa");

        System.out.println(map1.equals(map2));
        System.out.println(map1.keySet().equals(map2.keySet()));

        //Find extra key
        HashSet<String> combineKeys = new HashSet<>(map1.keySet());
        combineKeys.addAll(map2.keySet());
        combineKeys.removeAll(map1.keySet());
        System.out.println(combineKeys);

        //Compare map values - duplicates are allowed
        System.out.println(new ArrayList<>(map1.values()).equals(new ArrayList<>(map2.values()))); //false

        //Compare map values - duplicates are not allowed
        System.out.println(new HashSet<String>(map1.values()).equals(new HashSet<>(map2.values()))); //true
    }
}
